var worker = new Worker('asyncTask.js');
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
worker.addEventListener('message', messageHandler, false);

function messageHandler(message) {
    context.clearRect(0, 0, 500, 500);
    context.fillStyle = 'black';

    for (var i = 0, len = message.data.length; i < len; i++) {
        var body = message.data[i];
        context.fillRect(body.position.x, body.position.y, body.size.x, body.size.y);
    }
}