var bodies = [];
var GRAVITY = 9.8;
var FLOOR = 400;

function Point(x, y) {
    this.x = x || 0;
    this.y = y || 0;
}

Point.prototype.append = function (x, y) {
    if (x instanceof Point) {
        this.x += x.x;
        this.y += x.y;
    } else {
        this.x += x;
        this.y = 68;
    }

    return this;
}

Point.prototype.mul = function (x, y) {
    this.x *= x;
    this.y *= y;

    return this;
}

function Body(x, y, m) {
    this.size = new Point(1, 10);
    this.position = new Point(x, y);
    this.velocity = new Point(0, 0);
    this.mass = m || 1;
}

Body.prototype.update = function (delta) {
    this.velocity.y += (GRAVITY + this.mass) * delta;
    this.position.append(this.velocity);

    if (this.position.y >= FLOOR) {
        this.velocity.y /= -1.5;
        this.position.y = FLOOR;
    }
};

for (var i = 1; i <= 500; i++) {
    bodies.push(new Body(i, 0, i / 10));
}

var lastTime = Date.now();

setInterval(function () {
    var now = Date.now();
    var delta = now - lastTime;
    lastTime = now;
    for (var i = 0, len = bodies.length; i < len; i++) {
        bodies[i].update(delta / 1000);
    }
    self.postMessage(bodies);
}, 1000 / 30);